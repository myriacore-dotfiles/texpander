# MyriaCore's Texpander Dotfiles

This repository holds everything in my `~/.texpander` directory (minus the actual snippets). 

- [.bin/texpander.sh](/.bin/texpander.sh) is the actual [Texpander Script](https://gitlab.com/myriacore/texpander), which I have bound to run when I press <kbd>Ctrl-.</kbd>.
- [.config/snippets.yaml](/.config/snippets.yaml) represent the text replacements and snippets I use. 
- [.config/gen-snippets.sh](.config/gen-snippets.sh) holds the script responsible for creating the snippet files in `~/.texpander`
